import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
/**
 * expand
 */
  expand() {
  var continues = document.getElementById("continues");
  var readMoreBtn = document.getElementById("read-more-btn");
  var readMoreText = document.getElementById("read-more");

  if (continues.style.display === "none") {
    continues.style.display = "inline";
    readMoreText.style.display = "none";
  } else {
    continues.style.display = "none";
    readMoreText.style.display = "inline";
  }
}
}

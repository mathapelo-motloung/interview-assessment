import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import {Enquiry} from '../contact/Enquiry';
import {RestService} from '../rest.service'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],

})
export class ContactComponent implements OnInit {
  
  submitted = false;
  contactForm: FormGroup;

  constructor(private restservice: RestService, private formbuilder: FormBuilder) { }

  ngOnInit() {
    
    this.contactForm = this.formbuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone:['',[
        Validators.required,
        Validators.pattern("^[0-9]*$"),
        Validators.minLength(9),
        Validators.maxLength(9)]
      ],
      message:['', Validators.required]
    });
  }

  //Access form fields
  get form() {
    return this.contactForm.controls;
  }

  onSubmit() {
    
      this.submitted = true;
      document.getElementsByClassName('alert-sucees');
      return this.restservice.enquiryPost(this.contactForm.value).subscribe(res => console.log(res)); 
  
    
  }

  //Clear form after submission
  // onReset() {
  //   this.submitted = true;
  //   this.contactForm.reset();
  // }
}

export class Validate {
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }
}


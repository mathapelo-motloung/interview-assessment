export interface Recipe {
    title:String,
    description:String,
    ingredients:String,
    method:String,
    image:String
}

export interface RecipeResponse {
    recipes: Recipe[];
}

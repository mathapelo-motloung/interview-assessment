import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import { Observable, of } from 'rxjs';
import { map, catchError, tap, subscribeOn } from 'rxjs/operators';
import { Recipe, RecipeResponse } from './Recipe';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],

})
export class RecipeComponent implements OnInit {
  recipes: Recipe[];
  constructor(private RestService: RestService){

  }
  ngOnInit() {
    //console.log('rest service working');
    this.RestService
        .getRecipe()    
        .subscribe ((response:RecipeResponse)=>{
          // console.log('this is working', response.recipes);
          this.recipes = response.recipes;
    
    });
  }

  // RecipePage(){
  //     this.RestService
  //       .getRecipe()    
  // }

}

import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import { Gallery, GalleryImages } from './Gallery';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  galleries: Gallery[];

  constructor(private RestService: RestService) { }

  ngOnInit() {
    this.RestService
        .getGallery()
        .subscribe((image:GalleryImages)=>{
          this.galleries = image.galleries;
        });
     

  }

}

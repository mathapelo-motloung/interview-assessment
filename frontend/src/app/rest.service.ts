import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap, subscribeOn } from 'rxjs/operators';
import { Enquiry } from './contact/Enquiry';



@Injectable({
  providedIn: 'root'
})
export class RestService {

  url: string = 'http://localhost:4000';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      
    })
  };

  constructor(private http: HttpClient) {
    // console.log(this.url);
  }

  //******************************************************************
  getRecipe() {
    return this.http.get(this.url + '/api/recipe')
    // .subscribe(data => console.log(data));
  }
  getGallery() {
    return this.http.get(this.url + '/api/galleries')
  }
  enquiryPost (enquiry: Enquiry) {
    return this.http.post<Enquiry>(this.url + '/api/enquiry/add', enquiry, this.httpOptions);
  }
  
}

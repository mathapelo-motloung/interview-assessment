var keystone = require ('keystone');
var Types = keystone.Field.Types;
var Recipe = new keystone.List('Recipe');


Recipe.add({
    title: { type: String, required: true ,initial:'Recipe Name'},
    description: { type: Types.Textarea },
    ingredients: { type: Types.TextArray},
    method:{ type: Types.TextArray },
    image: { type: Types.CloudinaryImage }
});

Recipe.schema.virtual('canAccessKeystone').get(function(){
    return true;
});

Recipe.register();
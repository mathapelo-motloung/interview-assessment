var async = require('async'),
	keystone = require('keystone');

var Enquiry = keystone.list('Enquiry');

//Add post to database
exports.create = function(req, res){
  console.log('=== Enquiry Create ===');

  var enquiry = new Enquiry.model(req.body);

  console.log('<>', req.body);
  enquiry.save(function(err,data){
      if(err) {
        // console.error('<<<<', err);
        res.status(500).send({
          status: 'error',
          message: 'Unable to save data'
        });
      }
        //res.apiResponse({data});
        res.status(201).send({
          status: 'success',
          message: 'Successfully created new entry'
        })
    });
 
}
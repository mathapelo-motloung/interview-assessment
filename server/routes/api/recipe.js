var async = require('async'),
	keystone = require('keystone');

var Recipe = keystone.list('Recipe');

exports.get = function(req, res) {
	Recipe.model.find({},
		{
		  _id: false,
		  title: true,
		  description: true,
		  ingredients: true,
		  method: true,
		  image: true
		})
		.exec(function (err, recipes) {
		  if (err) {
			  return res.apiError('database error', err);
			}
		  if (recipes.length === 0) {
			res.status(404)
			return res.apiResponse({error:'Recipe list  found'});
		  } 
		  res.status(200)
		  res.apiResponse({ recipes });
  
		});
}



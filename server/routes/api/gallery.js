var async = require('async'),
	keystone = require('keystone');
var Gallery = keystone.list('Gallery');

exports.get = function(req, res){
    Gallery.model.find({},
        {
          _id: false,
          images: true
        })
        .exec(function (err, galleries) {
          if (err) {
            return res.apiResponse('Database error', err);
          }
          if (galleries.length === 0) {
            return res.apiResponse('No images found'); 
          }
          res.apiResponse({ galleries });
  
        });
}